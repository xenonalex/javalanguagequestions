
public class SolutionMain {

	public static void main(String[] args) {

		Cat a = new Cat();
		
		if(a instanceof Animal) {
			System.out.println("This is true...");
		}
		else {
			System.out.println("This is false...");
		}

	}

}