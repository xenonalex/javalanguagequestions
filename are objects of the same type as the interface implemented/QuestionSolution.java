interface Animal{
	public void sleeps();
}

class Cat implements Animal{
	
	public void sleeps() {
		System.out.println("I am sleeping now...");
	}
	
}