/***
 * Question: What�s the difference between an interface and an abstract class in Java?
 *  
 * Answer: It�s best to start answering this question with a brief definition of abstract 
 * classes and interfaces and then explore the differences between the two. A class must 
 * be declared abstract when it has one or more abstract methods. A method is declared 
 * abstract when it has a method heading, but no body � which means that an abstract 
 * method has no implementation code inside curly braces like normal methods do.
 * 
 * In the Figure class, we have an abstract method called getArea(), and because 
 * the Figure class contains an abstract method the entire Figure class itself must
 * be declared abstract. The Figure base class has two classes which derive from 
 * it � called Circle and Rectangle. Both the Circle and Rectangle classes provide definitions 
 * for the getArea() method.
 * 
 * But the real question is why did we declare the getArea method to be abstract in the 
 * Figure class? Well, what does the getArea method do? It returns the area of a specific 
 * shape. But, because the Figure class isn�t a specific shape (like a Circle or a Rectangle), 
 * there�s really no definition we can give the getArea method inside the Figure class. That�s 
 * why we declare the method and the Figure class to be abstract. Any classes that derive from 
 * the Figure class basically has 2 options: 1. The derived class must provide a definition 
 * for the getArea method OR 2. The derived class must be declared abstract itself.
 * 
 * A non abstract class is called a concrete class: You should also know that any non abstract 
 * class is called a concrete class. Knowing your terminology definitely pays off in an interview.
 */
public class AbstractDemoMain {

	public static void main(String[] args) {

		Circle a = new Circle(12.5);
		Rectangle b = new Rectangle(5, 3);
		
		System.out.print(a.getArea() + "\n");
		System.out.print(b.getArea() + "\n");
		
	}

}
