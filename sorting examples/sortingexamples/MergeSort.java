package sortingexamples;
/***
 * This is another divide-and-conquer algorithm: Split the list into two, sort each sublist, and then
 * merge the two lists together.
 * 
 * The main code is in merging the two lists efficiently. The pseudocode holds a pointer to each sublist,
 * adds the lowest value pointed to, and then increments the appropriate pointer. Once one pointer
 * reaches the end of its list, you can add all of the remaining elements from the other list. From the
 * second and third while statements in the preceding merge method, one while statement will immediately
 * return false, because all the elements will have been consumed from that sublist in the original
 * while statement.
 * 
 * There should be no surprise here; it is very similar to the pseudocode. Note that the subList
 * method on List takes two parameters, from and to: from is inclusive, and to is exclusive.
 * 
 * A common question in interviews is often to ask you to merge two sorted lists, as was shown in the
 * merge method in the preceding listing.
 * 
 * Again, merge sort has a performance of O(n log n). Each merge operation is O(n), and each recursive
 * call works on only half of the given list.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSort {

	public static void main(String[] args) {
		
		final List<Integer> input = new ArrayList<>();
		
		input.add(5);
		input.add(2);
		input.add(9);
		input.add(6);
		input.add(0);
		input.add(1);
		input.add(8);
		
		System.out.println(Arrays.deepToString(mergeSort(input).toArray()));
		
	}

	public static List<Integer> mergeSort(final List<Integer> values) {
		if (values.size() < 2) {
			return values;
		}
		
		final List<Integer> leftHalf = values.subList(0, values.size() / 2);
		final List<Integer> rightHalf = values.subList(values.size() / 2, values.size());
		
		return merge(mergeSort(leftHalf), mergeSort(rightHalf));
		
	}
	
	private static List<Integer> merge(final List<Integer> left, final List<Integer> right) {
		int leftPtr = 0;
		int rightPtr = 0;
		final List<Integer> merged = new ArrayList<>(left.size() + right.size());
		
		while (leftPtr < left.size() && rightPtr < right.size()) {
			
			if (left.get(leftPtr) < right.get(rightPtr)) {
				
				merged.add(left.get(leftPtr));
				leftPtr++;
				
			} 
			else {
		
				merged.add(right.get(rightPtr));
				rightPtr++;
			}
		}
		
		while (leftPtr < left.size()) {
			merged.add(left.get(leftPtr));
			leftPtr++;
		}
		
		while (rightPtr < right.size()) {
			merged.add(right.get(rightPtr));
			rightPtr++;
		}
		
		return merged;
	}
}
