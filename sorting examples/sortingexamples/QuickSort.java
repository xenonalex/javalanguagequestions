/***
 * This algorithm is recursive. The base case is when a list is provided with zero or one element. You
 * can simply return that list because it is already sorted.
 * The second part of the algorithm is to pick an arbitrary element from the list, called the pivot. In
 * this case the first element in the list was used, but any element could have been picked. The remaining
 * elements are separated into two: those lower than the pivot, and those equal to or larger than
 * the pivot.
 * 
 * The method is then called on each of the two smaller lists, which will return in each segment being
 * sorted. The final list is the smaller elements, now sorted, followed by the pivot, and then the higher
 * sorted elements.
 * 
 * If you ever write a recursive algorithm, you must make sure that it terminates. The algorithm in
 * Listing 4-8 is guaranteed to terminate, because each recursive call is made with a smaller list, and
 * the base case is on a zero- or one-element list.
 * 
 * The performance of Listing 4-8 is much more efficient than the bubble sort and insertion sort algorithms.
 * The separation of the elements into two separate lists is O(n), and each recursive call happens
 * on half of each list, resulting in O(n log n). This is an average performance. The worst case is
 * still O(n2). The choice of the pivot can make a difference: For the implementation given here, if you
 * always pick the first element as the pivot, and the list is ordered in reverse, then the recursive steps
 * only reduce by one each time.
 * 
 * It is worth noting that each division of the list and the subsequent recursive call is independent of
 * any other sorting necessary, and could be performed in parallel.
 */

package sortingexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuickSort {

	public static void main(String[] args) {

		List<Integer> input = new ArrayList<>();
		input.add(7);
		input.add(2);
		input.add(8);
		input.add(5);
		input.add(1);
		input.add(3);
		input.add(0);

		System.out.println(Arrays.deepToString(quickSort(input).toArray()));
	}

	public static List<Integer> quickSort(List<Integer> numbers) {
		if (numbers.size() < 2) {
			return numbers;
		}
		
		final Integer pivot = numbers.get(0);
		final List<Integer> lower = new ArrayList<>();
		final List<Integer> higher = new ArrayList<>();
		
		for (int i = 1; i < numbers.size(); i++) {
			if (numbers.get(i) < pivot) {
				lower.add(numbers.get(i));
			} 
			else {
				higher.add(numbers.get(i));
			}
		}
		
		final List<Integer> sorted = quickSort(lower);
		sorted.add(pivot);
		sorted.addAll(quickSort(higher));
		
		return sorted;
	}
}
