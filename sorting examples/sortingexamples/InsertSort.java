/***
 * There are several points to note about this implementation. Notice that the method returns a new
 * List unlike the bubble sort, which sorted the elements in place. This is mainly the choice of the
 * implementation. Because this algorithm created a new List, it made sense to return that.
 * Also, the list implementation returned is a LinkedList instance. A linked list is very efficient in
 * adding elements in the middle of the list, simply by rearranging the pointers of the nodes in the list.
 * 
 * If an ArrayList had been used, adding elements to the middle would be expensive. An ArrayList
 * is backed by an array, so inserting at the front or middle of the list means that all subsequent elements
 * must be shifted along by one to a new slot in the array. This can be very expensive if you
 * have a list with several million rows, especially if you are inserting early in the list. If the difference
 * between list data structures, such as array lists and linked lists, are new to you, look at Chapter 5
 * on data structures and their implementations.
 * 
 * Finally, the outer loop in the implementation is labeled originalList. When an appropriate place
 * to insert the element has been found, you can move on to the next element in the original array.
 * Calling continue inside a loop will move on to the next iteration of the enclosing loop. If you want
 * to continue in an outer loop, you need to label the loop and call continue with its identifier.
 * 
 * As hinted at with the outer loop labeling and the continue statement, once the element has successfully
 * been placed, the algorithm moves on to the next element. This is an advantage over bubble
 * sort: Once the list to return has been constructed, it can be returned immediately; there are no
 * redundant cycles to check that the list is in order.
 * 
 * The worst-case performance for this algorithm is still O(n2), though: If you attempt to sort an
 * already-sorted list, you need to iterate to the end of the new list with each element to insert.
 * Conversely, if you sort a reverse-order list, you will be putting each element into the new list at the
 * head of the list, which is O(n).
 * 
 * The algorithm described here uses twice as much space to sort the list because a new list is returned.
 * The bubble sort algorithm only uses one extra slot in memory, to hold the value temporarily while
 * being swapped.
 */
package sortingexamples;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class InsertSort {

	public static void main(String[] args) {
		final List<Integer> input = new LinkedList<>();
		input.add(5);
		input.add(2);
		input.add(9);
		input.add(6);
		input.add(0);
		input.add(1);
		input.add(8);
		
		System.out.println(Arrays.deepToString(insertSort(input).toArray()));
	}
	
	public static List<Integer> insertSort(final List<Integer> numbers) {
		final List<Integer> sortedList = new LinkedList<>();
		
		originalList: for (Integer number : numbers) {
			
			for (int i = 0; i < sortedList.size(); i++) {
				
				if (number < sortedList.get(i)) {
					
					sortedList.add(i, number);
					continue originalList;
					
				}
			}
			
			sortedList.add(sortedList.size(), number);
		}
		
		return sortedList;
	}

}
