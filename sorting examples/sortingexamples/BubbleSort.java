package sortingexamples;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BubbleSort {

	public static void main(String[] args) {
		
		int[] input1 = {5,3,7,1,9,2,0};
		int[] input2 = {1,2,3,4,7,5};
		
		System.out.println(bubbleSort(input2));
		
	}
	
	/**
	 * This is a bubble sort in ascending order.
	 */
	private static String bubbleSort(int[] intlist) {
		
		boolean isNumbersSwitched;
		
		do {
			isNumbersSwitched = false;
			
			for(int i = 0; i < intlist.length -1; i++) {
				
				//if the later value is less than the current value, swap the values
				if(intlist[i + 1] < intlist[i]) {
					int temp = intlist[i + 1];
					intlist[i + 1] = intlist[i];
					intlist[i] = temp;
					
					//once a value swap occured in ANY part of the loop, this will be tagged as true.
					isNumbersSwitched = true;
				}
			}	
		}
		
		/*
		 * The loop will continue to run while there are still swapping occuring in every loop iteration.
		 * Once there are no swapping occured in a loop iteration, it means that the array list sorting
		 * has been completed, tagging the isNumbersSwitched to false that will break the while loop and
		 * left with the sorted array.
		 */
		while(isNumbersSwitched);
		
		return Arrays.toString(intlist);
	}

}
