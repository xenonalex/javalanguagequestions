/***
 * When searching through a list, unless the list is sorted in some fashion, the only sure way to find a
 * given value is to look at every value in the list.
 * 
 * But if you are given a sorted list, or if you sort the list before searching, a binary search is a very efficient
 * method to see if a given value is in the list
 * 
 * The beauty of this algorithm is that you use the property of the sorted list to your advantage. You
 * can throw away many elements without even examining them, because you know they definitely
 * cannot be equal to the given element. If you have a list with one million elements, you can find a
 * given element in as little as twenty comparisons. This algorithm has the performance of O(n).
 */

package sortingexamples;

import java.util.LinkedList;
import java.util.List;

public class BinarySearch {

	public static void main(String[] args) {
		final List<Integer> sortedList = new LinkedList<>();
		sortedList.add(0);
		sortedList.add(1);
		sortedList.add(2);
		sortedList.add(5);
		sortedList.add(6);
		sortedList.add(8);
		sortedList.add(9);

		final Integer searchValue = 8;
		
		System.out.println(Boolean.toString(binarySearch(sortedList, searchValue)));
	}
	
	public static boolean binarySearch(final List<Integer> numbers, final Integer value) {
		if (numbers == null ||numbers.isEmpty()) {
			return false;
		}
			
		final Integer comparison = numbers.get(numbers.size() / 2);
		
		if (value.equals(comparison)) {
			return true;
		}
			
		if (value < comparison) {
			return binarySearch(numbers.subList(0, numbers.size() / 2),value);
		} 
		else {
			return binarySearch(numbers.subList(numbers.size() / 2 + 1, numbers.size()),value);
		}
	}

}
