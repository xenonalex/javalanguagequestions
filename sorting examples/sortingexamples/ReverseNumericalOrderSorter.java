package sortingexamples;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

/***
 * Doing a reverse numerical sorting order using the Comparator interface
 *
 */
public class ReverseNumericalOrderSorter implements Comparator<Integer>{

	//This is will sort the list in Decending order
	@Override
	public int compare(Integer o1, Integer o2) {
		return o2 - o1;
	}
	
	@Test
	public void customSorting() {
		final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
		final List<Integer> expected = Arrays.asList(7, 6, 5, 4, 4, 3, 1);
		
		numbers.sort(new ReverseNumericalOrderSorter());
		
		//If you want to use the default sorting order (Ascending), pass null to the sort() method
		//numbers.sort(null);
		
		assertEquals(expected, numbers);
	}
	
}