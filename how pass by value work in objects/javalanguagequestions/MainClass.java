package javalanguagequestions;

/***
 * Question: Are objects passed by reference in Java?
 * 
 * Answer: As we said in the beginning of this article � everything is passed by value in Java. So, 
 * objects are not passed by reference in Java. Let�s be a little bit more specific by what we mean 
 * here: objects are passed by reference � meaning that a reference/memory address is passed when an 
 * object is assigned to another � BUT (and this is what�s important) that reference is actually passed 
 * by value. The reference is passed by value because a copy of the reference value is created and passed 
 * into the other object � read this entire article and this will make a lot more sense. So objects are 
 * still passed by value in Java, just like everything else. One other very important thing to know is 
 * that a variable of a class type stores an address of the object, and not the object itself. The object 
 * itself is stored at the address which the variable points to.
 * 
 * Question: Is it possible to pass an object by reference in Java?
 * 
 * Answer: No, there are no �extra� language features in Java that would allow for you to pass an object by 
 * reference in Java. Java is strictly pass by value, and does not give the programmer the option of passing 
 * anything by reference.
 */
public class MainClass {

	public static void main(String[] args) {
		
		BaseClass var1 = new BaseClass("Lex");
		BaseClass var2;
		
		var2 = var1;
		
		BaseClass var3 = new BaseClass("Ramos");
		
		var1 = var3;
		
		//WHAT IS OUTPUT BY THIS?
		System.out.print(var2.getName());
		System.out.println(var1.getName());

	}

}
