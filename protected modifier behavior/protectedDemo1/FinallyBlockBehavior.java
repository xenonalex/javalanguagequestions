package protectedDemo1;

public class FinallyBlockBehavior {

    public static void main(String args[]) 
    { 
        // call the proveIt method and print the return value
    	System.out.println(FinallyBlockBehavior.finallyDemo1());
    }

    /***
     * Question:In Java, will the code in the finally block be called 
     * and run after a return statement is executed?
     * 
     * Answer: yes � the code in a finally block will take precedence over the return statement.
     * 
     * Question: What is the very unique situations when finally will not run after return?
     * 
     * Answer:The finally block will not be called after return in a couple of unique scenarios: 
     * if System.exit() is called first, or if the JVM crashes.
     */
    public static int finallyDemo1()
    {
    	try {  
            	return 1;  
    	}  
    	finally {  
    	    System.out.println("finally block is run before method returns.");
    	}
    }

    /***
     * Question: What if there is a return statement in the finally block as well?
     * 
     * Answer: Anything that is returned in the finally block will actually override 
     * any exception or returned value that is inside the try/catch block.
     */
    public static int finallyDemo2(){
        try{
            return 7;
        } 
        finally{
            return 43;
        }
    }
    
    /***
     * if the finally block returns a value, it will override any exception thrown 
     * in the try/catch block.
     */
    public static int finallyDemo3(){
        try{
            throw new NoSuchFieldException();
        } 
        finally{
            return 43;
        }
    }
}
