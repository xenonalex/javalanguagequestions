/***
 * Question: What is the diamond problem? 
 * 
 * Answer: We have 2 classes B and C that derive from the same class  which would be class A. 
 * We also have class D that derives from both B and C by using multiple inheritance. Illustrating 
 * these relationships of the classes essentially form the shape of a diamond  which is why this 
 * problem is called the diamond problem.
 * 
 * The problem with having an inheritance hierarchy like this is that when we instantiate an object 
 * of class D, any calls to method definitions in class A will be ambiguous  because its not sure 
 * whether to call the version of the method derived from class B or class C.
 * 
 * Question: Does it exist in Java? If so, how can it be avoided?
 * 
 * Answer: 
 * - From a class standpoint, No. Since Java does not allow multiple inheritance which the diamond problem presents.
 * - From a interface standpoint, creating a relationship like the diamond problem is possible. HOWEVER, Although 
 * interfaces give us something similar to multiple inheritance, the implementation of those interfaces is singly 
 * (as opposed to multiple) inherited. This means that problems like the diamond problem  in which the compiler 
 * is confused as to which method to use  will not occur in Java.
 */


interface InterfaceA{
	
	public void methodA();
	
}

interface InterfaceB extends InterfaceA{
	
	public void methodB();
	
}

interface InterfaceC extends InterfaceA{
	
	public void methodC();
	
}

interface InterfaceD extends InterfaceB, InterfaceC{
	
	
	public void methodD();
}

class DiamondProblem implements InterfaceD{
	
	public static void main(String[]arg) {
		//some logic here...
	}
	
	public void methodA() {
		System.out.println("This is methodA");
	}
	
	public void methodB() {
		System.out.println("This is methodB");
	}
	
	public void methodC() {
		System.out.println("This is methodC");
	}
	
	public void methodD() {
		System.out.println("This is methodD");
	}
	
}