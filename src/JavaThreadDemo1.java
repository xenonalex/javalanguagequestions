/***
 * Question: What are the two different methods of creating a thread in Java? 
 * What method must be implemented in either case?
 *  
 * Answer: One way of creating a thread in Java is to define a class that is a 
 * derived from the Thread class which is built into Java. An object of this derived class will 
 * be a thread.
 * 
 * The next question is once we derive from the Thread class, where do we actually do the 
 * programming for that thread? Well, the Thread class has a method called run, which you 
 * must override in order to have your thread do whatever you want it to do.
 * 
 *  to create a thread you can either extend from the Thread class or implement the Runnable 
 *  interface. As you�ve probably noticed, in either case you must implement the "run" method.
 */
public class JavaThreadDemo1 extends Thread{

	public void run(){
		System.out.print("This is my thread that came from JavaThreadDemo1 \n");
	}
	
}
