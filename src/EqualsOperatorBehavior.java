/***
 * Question: Whats the difference between equals() and ==?
 */
public class EqualsOperatorBehavior {

	public static void main(String[] args) {
		String a = new String("Lex");
		String b = new String("Lex");
		String c = new String("Ramos");
		String d = new String("Ramos");
		
		
		/**
		 * The == operator
		 * 
		 * In Java, when the == operator is used to compare 2 objects, it checks to see if the 
		 * objects refer to the same place in memory. In other words, it checks to see if the 2 
		 * object names are basically references to the same memory location.
		 * 
		 * Take a guess at what the code above will output. Did you guess that it will output 
		 * a==b is TRUE? Well, if you did, then you are actually wrong. Even though the strings have 
		 * the same exact characters (Lex), The code above will actually output false.
		 */
		if(a == b) {
			System.out.println("1. Comparison by == : true...");
		}
		else {
			System.out.println("1. Comparison by == : false...");
		}

		/**
		 * Note in the code that a and b both reference the same place in memory because of this line: a = b;. 
		 * And because the == compares the memory reference for each object, it will return true. And, the 
		 * output of the code above will be true.
		 */
		a = b;
		
		if(a == b) {
			System.out.println("1. Comparison by == : true...");
		}
		else {
			System.out.println("1. Comparison by == : false...");
		}
		
		/**
		 * The equals() method
		 * 
		 * Now that weve gone over the == operator, lets discuss the equals() method and how that compares to 
		 * the == operator. The equals method is defined in the Object class, from which every class is either 
		 * a direct or indirect descendant. By default, the equals() method actually behaves the same as the == 
		 * operator  meaning it checks to see if both objects reference the same place in memory. But, the equals 
		 * method is actually meant to compare the contents of 2 objects, and not their location in memory.
		 */
		if(c.equals(d)) {
			System.out.println("3. Comparison by .equals() = : true...");
		}
		else {
			System.out.println("3. Comparison by .equals() : false...");
		}
		
	}

}
