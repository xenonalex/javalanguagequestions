/***
 * We�ve now seen that we can create a thread by deriving from the Thread class, 
 * so what�s the other way of creating a thread in Java?
 *  
 *Java does not support multiple inheritance, which means that you can not derive 
 *from multiple classes at once. So, there may be a situation in which you want to 
 *create a thread, but you also want to derive from a class that is not the Thread 
 *class. For this reason, Java provides an alternative way of creating a thread � 
 *so that you don�t have to create a class that derives from the Thread class. 
 *Java provides an interface called Runnable that your class can implement in order 
 *to become a thread. The Runnable interface has only one method heading that you 
 *must implement yourself: public void run();
 */
public class JavaThreadDemo2 implements Runnable{
	
	public void run(){
		/* This is where you would define the behavior of your thread, just
		   as if this class were derived from the Thread class
		*/
		System.out.print("This is my thread that came from JavaThreadDemo2 \n");
	}
	
	/***
	 * If you create a class that implements the Runnable interface then that 
	 * class must still be run from an instance of the Thread class. You can 
	 * accomplish this by passing the Runnable object as an argument to the Thread constructor.
	 */
	public void startThread()
	{
		/* Note below how we pass the current instance of the
		   ThreadClass (by using the "this" parameter) to the 
		   Thread constructor and then calling the run method
		   to actually start the thread
		*/	
		
		Thread aThread = new Thread(this);
		aThread.run();
	}
	
}
