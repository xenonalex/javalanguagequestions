/***
 * Question: In Java, what does it mean if a class is serializable � 
 * provide an example as well of a serializable class? Also, what is a transient variable?
 *  
 *Answer: In Java, a class is serializable if and when it implements the Serializable 
 *interface, which is an interface residing in the java.io package. If a class is 
 *serializable, it means that any object of that class can be converted into a 
 *sequence of bits so that it can be written to some storage medium (like a file), or 
 *even transmitted across a network. Here�s what a serializable class looks like:
 */
public class SerializableDemo1 implements java.io.Serializable{
	// this class is serializable
}
