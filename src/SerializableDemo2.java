/***
 * Question: What is a transient variable?
 * 
 * Answer: Suppose that there�s a particular object data member (like a password) 
 * that we may not want to get saved when an object is serialized. Then, what we 
 * can do is declare that data member to be "transient". A transient variable 
 * is not a part of the persistent state of a serialized object. So, if we were 
 * to write a serializable object to a file, any transient variable that was 
 * part of that object will not be saved to the file.
 *
 */
public class SerializableDemo2 {
    // this variable will not persist
	private transient String password; 	
}
