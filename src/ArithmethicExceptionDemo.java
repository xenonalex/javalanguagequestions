/***
 * Question: What�s the result of running the following code? Is an exception thrown? 
 * If so, which exception is thrown?
 *  
 * Answer: ArithmeticException is thrown. The exception that gets thrown when a division 
 * by 0 is done is the ArithmeticException. So, the first thing that gets printed out 
 * is "arithmetic error".
 * 
 * The next question is whether the finally block actually executes. Yes, the finally block will 
 * execute in this code.
 * 
 * It�s worth noting that if we didn�t have the "catch(ArithmeticException e)" block in the code, 
 * then the exception would have just been caught by the "catch(Exception e)" block and the output 
 * would have been "generic exception. In the finally block".
 */
public class ArithmethicExceptionDemo {

	public static void main(String[] args) {

		int i=1;
		int j=1;

		try
		{
			i++; //becomes 2
			j--; //becomes 0
			
			if (i/j > 1)
			{
				i++;
			}
		}

		catch(ArithmeticException e)
		{				
			System.out.println("arithmetic error.");
		}

		catch(ArrayIndexOutOfBoundsException e)
		{

			System.out.println("Array index out of bounds");
		}

		catch(Exception e)
		{		
			System.out.println("generic exception.");
		}

		finally
		{

			System.out.println("In the finally block");
			
		}
	}
}
