public class JavaTreadDemoMain {

	public static void main(String[] args) {
		
		/***
		 * if you want to actually create a thread object
		 * then you would do something like this:
		 */
		JavaThreadDemo1 thread1 = new JavaThreadDemo1();
		JavaThreadDemo2 thread2 = new JavaThreadDemo2();
		
		/* this call will create a new independent thread
		   called derivedThread, and will start its processing
		*/
		thread1.start();
		thread2.startThread();
		
	}

}
