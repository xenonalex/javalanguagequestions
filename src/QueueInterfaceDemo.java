/***
 * A Queue is a Java interface that represents a �first in, first out� data structure. The interface has the
 * methods add, to add a new element, remove to remove the oldest element, and peek, which returns
 * the oldest element, but does not remove it from the data structure.
 * The LinkedList class implements the Queue interface.
 */

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

public class QueueInterfaceDemo {

	@Test
	public void queueDemonstration() {
		Queue<String> queueList = new LinkedList<>();
		
		queueList.add("First");
		queueList.add("Second");
		queueList.add("Third");
		
		assertEquals("First", queueList.remove());
		assertEquals("Second", queueList.remove());
		assertEquals("Third", queueList.remove());
		
	}
	
}
