import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MapDemonstration {

	@Test
	public void mapTest() {
		
		Map<String,String> map1 = new HashMap<>();
		
		map1.put("like", "jacuzzi");
		map1.put("dislike", "steam room");
		
		assertEquals("jacuzzi", map1.get("like"));
		
		//One property of a map is that the value of a key can only appear once: Reinserting that key will
		//result in the original value being overwritten
		map1.put("like", "sauna");
		
		assertEquals("sauna", map1.get("like"));
		
	}
	
}
